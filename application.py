#!/usr/bin/python
# Copyright Sumana Harihareswara, GPL, thanks to https://github.com/bev-a-tron/MyFlaskTutorial

# This application is a reimplementation of http://kevan.org/catfishing.php

import random

from flask import Flask,render_template,request,redirect,url_for
app_fl = Flask(__name__)

app_fl.vars = {}

@app_fl.route('/',methods=['GET'])
def index_pg():
    sugg = choosebasicright(universalrights[1:])
    return render_template('simple.html',right=sugg)

@app_fl.route('/about',methods=['GET'])
def about_pg():
    return render_template('about.html')

@app_fl.route('/custom',methods=['GET','POST'])
def custom_pg():
    if request.method == 'GET':
        sugg = makemultirightslist([universalrights, childrenrights, womenrights], True)
        return render_template('bespoke.html',rights=sugg, sexist=True, ageist=True, boringfilter=True)
    else:
        #request was a POST
        app_fl.vars["sexist"] = request.form.get("sexist")
        app_fl.vars["ageist"] = request.form.get("ageist")
        if request.form.get("boringfilter") == "True":
            app_fl.vars["boringfilter"] = True
        else:
            app_fl.vars["boringfilter"] = False
        if app_fl.vars["sexist"] and app_fl.vars["ageist"]:
            sugg = makemultirightslist([universalrights, childrenrights, womenrights], app_fl.vars["boringfilter"])
        elif app_fl.vars["ageist"]:
            sugg = makemultirightslist([universalrights, universalrights, childrenrights], app_fl.vars["boringfilter"])
        elif app_fl.vars["sexist"]:
            sugg = makemultirightslist([universalrights, universalrights, womenrights], app_fl.vars["boringfilter"])
        else:
            sugg = makemultirightslist([universalrights, universalrights, universalrights], app_fl.vars["boringfilter"])
        return render_template('bespoke.html',rights=sugg,sexist=app_fl.vars["sexist"], ageist=app_fl.vars["ageist"], boringfilter=app_fl.vars["boringfilter"])

#import logging
#file_handler = logging.FileHandler(filename='/home/sumanah/dystopia.log')
#file_handler.setLevel(logging.WARNING)
#app_fl.logger.addHandler(file_handler)

if __name__ == '__main__':
    app_fl.run(debug=False)
