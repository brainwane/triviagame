This is a reimplementation of Kevan's Catfishing game http://kevan.org/catfishing.php using Wikipedia categories -- now, Wikidata categories -- as trivia clues.

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
